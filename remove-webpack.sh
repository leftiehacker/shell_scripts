#!/bin/bash

# Notes:
  # Undoes ./backend-webpack.sh, ./library-webpack.sh, etc.

sudo npm uninstall @babel/core @babel/preset-env babel-loader webpack webpack-cli
rm webpack.config.js
json -I -f package.json -e 'this.scripts.webpack="echo \"Webpack was removed from this project.\""'