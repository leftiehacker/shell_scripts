# Convert every local GitHub repo on 'origin'
# to GitLab repo on 'gitlab'

read -p "GitLab username: " gitlab_username

# for every directory in the parent directory
for d in ./*/ 
do ( 
    # enter the directory
    cd "$d"

    # get the git url associated with 'origin'
    url=$(git remote get-url origin)

    # when url contains 'github'
    if [[ $url = *"github"* ]]; then
      # in case gitlab remote pre-exists
      git remote remove gitlab

      # convert github url to gitlab url
      gitlab_url=${url/github/gitlab}
      username_url=$(echo $gitlab_url | sed "s|/.[^/]*|/$gitlab_username|2")

      # add new remote url
      git remote add gitlab $username_url
    fi
) done

