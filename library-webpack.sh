#!/bin/bash

# Notes:
  # Installs all dependencies for compiling ES6+ files with Babel and Webpack
  # Creates webpack config with @babel/preset-env, outputs to /build/bundle.js
  # Run after npm init of the target directory

isInstalled=$(json --version | grep 'Trent Mick')

if [ "$isInstalled" != 'written by Trent Mick' ];then
  sudo npm i -g json
fi

sudo npm i @babel/core @babel/preset-env babel-loader@^8.0.0-beta webpack --save-dev

read -p "Name of your webpack entry point (e.g. index.js): " entry
read -p "Name of your library: " library

echo "
const webpack = require('webpack')
const path = require('path')
const fs = require('fs');
const nodeModules = {};

fs.readdirSync('node_modules')
  .filter(x => ['.bin'].indexOf(x) === -1)
  .forEach(mod => nodeModules[mod] = 'commonjs ' + mod);
  
module.exports = {
  entry: './$entry',
  target: 'node',
  externals: nodeModules,
  output: {
    path: path.resolve(__dirname),
    filename: 'bundle.js',
    library: '$library',
    libraryTarget: 'commonjs2'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          },
        },
      },
    ],
  },
};
" > webpack.config.js

json -I -f package.json -e 'this.scripts.webpack="webpack --watch --progress"'
npm run webpack